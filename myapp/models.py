from django.db import models
from django.forms import ModelForm


class AttendanceCoursePoints(models.Model):
    course_id = models.CharField(max_length=100, blank=True, null=True)
    category = models.CharField(max_length=3)
    points = models.FloatField(blank=True, null=True)
    description = models.CharField(max_length=100, blank=True, null=True)
    is_active = models.BooleanField(default=False,blank=True, null=True)
    is_default = models.IntegerField(default=0,blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'attendance_course_points'


class AttendanceCourseSession(models.Model):
    course_id = models.CharField(max_length=100, blank=True, null=True)
    section_id = models.IntegerField()
    start_date = models.DateField(blank=True, null=True)
    start_time = models.TimeField(blank=True, null=True)
    end_time = models.TimeField(blank=True, null=True)
    description = models.CharField(max_length=255)
    title = models.CharField(max_length=50, blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'attendance_course_session'

class AttendanceUser(models.Model):
    user_id=models.IntegerField()
    user_email=models.EmailField()
    course_id = models.CharField(max_length=100, blank=True, null=True)
    user_name=models.CharField(max_length=100)
    is_present=models.IntegerField(default=0,blank=True, null=True)
    is_absent=models.IntegerField(default=0,blank=True, null=True)
    is_excused=models.IntegerField(default=0,blank=True, null=True)
    is_leave=models.IntegerField(default=0,blank=True, null=True)
    comment=models.CharField(max_length=200)
    #c=models.ForeignKey(AttendanceCourseSession,on_delete=models.CASCADE,null=True,blank=True)



class AttendanceHistory(models.Model):
    datetime = models.DateTimeField()
    event = models.CharField(max_length=50)
    course_id = models.CharField(max_length=50)
    section_id = models.IntegerField(blank=True, null=True)
    session_id = models.IntegerField(blank=True, null=True)
    user_ip = models.CharField(max_length=100, blank=True, null=True)
    data = models.TextField()
    user=models.ForeignKey(AttendanceUser,on_delete=models.CASCADE,null=True,blank=True)

    class Meta:
        managed = True
        db_table = 'attendance_history'


class AttendanceUserSessionPoints(models.Model):
    user = models.ForeignKey(AttendanceHistory, models.DO_NOTHING)
    course_session = models.ForeignKey(AttendanceCourseSession, models.DO_NOTHING, blank=True, null=True)
    course_points = models.ForeignKey(AttendanceCoursePoints, models.DO_NOTHING, blank=True, null=True)
    comments = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'attendance_user_session_points'
