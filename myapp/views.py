from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponse
from .forms import *
from .models import *
from django.contrib import messages
# Create your views here.

def getConfigurationAll(request):
    cpoints = AttendanceCoursePoints.objects.all()
    return render(request,"index.html",{'points':cpoints})


#
# def getConfigurationByid(request,course_id):
#     a = AttendanceCoursePoints.objects.filter(course_id=course_id)
#     form = CourseForm(request.POST, instance=a)
#     return render(request,"edit.html",{'formset':form,'course_id':course_id})

# def getConfigurationByid(request,course_id):
#   if request.method == 'POST':
#       form = EditResults(request.POST)
#       if form.is_valid():
#           print("inside if")
#           form.save()
#           return render(request, 'edit.html', {"formset": form})
#   else:
#       results = AttendanceCoursePoints.objects.filter(course_id=course_id)
#       form = EditResults(queryset=results)
#   return render(request, 'edit.html', {"formset": form,"course_id":course_id})



def getConfigurationByid(request,course_id):
    results = AttendanceCoursePoints.objects.filter(course_id=course_id)
    form = EditResults(queryset=results)
    return render(request, 'edit.html', {"formset": form,"course_id":course_id})


# def updateConfigurationPoints(request,course_id):
#     if request.method == 'POST':
#         form = EditResults(request.POST)
#         if form.is_valid():
#             form.save()
#             return HttpResponse("Sucess")
#     return render(request,'edit.html')



def updateConfigurationPoints(request,course_id):
    if request.method == 'POST':
        form = EditResults(request.POST)
        try:
            form.save()
        except ValueError as error:
            print(error)
        return HttpResponse("Sucess")
    return render(request,'edit.html')


# def updateConfigurationPoints(request,course_id):
#     if request.method == 'POST':
#         qa = AttendanceCoursePoints.objects.filter(course_id=course_id)
#         for i in qa:
#             form = Course_PointsForm(request.POST)
#             if form.is_valid():
#                 form.save()
#         return redirect("/show")
#     return render(request,"show.html")


# def updateConfigurationPoints(request,course_id):
#     cpoints = AttendanceCoursePoints.objects.filter(course_id=course_id)
#     form =Course_PointsForm(request.POST, instance=cpoints)
#     if form.is_valid():
#         form.save()
#         return redirect("/show")
#     return render(request,"show.html",{'points':cpoints})
#
# def updateConfigurationPoints(request,course_id):
#     if request.method == 'POST':
#         print(course_id)
#         qa = AttendanceCoursePoints.objects.filter(course_id=course_id)
#         print(request.POST)
#         for i in qa:
#             new_category= request.POST['category']
#             new_description=request.POST['description']
#             new_points=request.POST['points']
#             new_is_active=request.POST['is_active']
#             new_is_default=request.POST['is_default']
#             i.category=new_category
#             i.description=new_description
#             i.points=new_points
#             i.is_active=new_is_active
#             i.is_default=new_is_default
#             i.save()
#         return redirect("/show")
#     return render(request,"show.html")


def getAttendanceHistory(request,course_id):
    results = AttendanceHistory.objects.filter(course_id=course_id).values('user__user_email','event','section_id','session_id','user_ip')
    return render(request, 'history.html', {"history": results})


def getAllSession(request,course_id):
    result=AttendanceCourseSession.objects.filter(course_id=course_id)
    return render(request,'session.html',{"session":result,"course_id":course_id})

def addSession(request,course_id):
    if request.method=='POST':
        se=request.POST['title']
        dt=request.POST['sdate']
        sdt=request.POST['stime']
        edt=request.POST['etime']
        desc=request.POST['description']
        ssid=AttendanceCourseSession.objects.all().last()
        print(ssid.section_id)
        redf=ssid.section_id+1
        refvar=AttendanceCourseSession(course_id=course_id,section_id=redf,
                                start_date=dt,start_time=sdt,end_time=edt,
                                description=desc,title=se)
        refvar.save()
        return HttpResponse('saved')
    if request.method=='GET':
        return render(request,'attendance.html')



def recordAttendance(request,course_id):
    if request.method=='GET':
        data=AttendanceUser.objects.filter(course_id=course_id)
        form=EditUser(queryset=data)
        return render(request,"add.html",{"formset":form})

    if request.method=='POST':
        form=EditUser(request.POST)
        try:
            form.save()
        except ValueError as error:
            print(error)
        return HttpResponse("update")
    return HttpResponse("fail")