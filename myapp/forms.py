from django.forms import modelformset_factory, CheckboxInput, RadioSelect, NumberInput, TextInput

from .models import *
from django import forms

class Recordform(forms.ModelForm):
    class Meta:
        model=AttendanceUser
        fields='__all__'




# class CourseForm(forms.ModelForm):
#     course_id=forms.CharField(widget=forms.TextInput())
#     category=forms.CharField(widget=forms.TextInput())
#     points=forms.CharField(widget=forms.NumberInput())
#     description=forms.CharField(widget=forms.TextInput())
#     is_active=forms.CharField(widget=forms.CheckboxInput())
#     is_default = forms.CharField(widget=forms.RadioSelect())
#
#     class Meta:
#         model = AttendanceCoursePoints
#         fields = ('id','course_id','category','points','description','is_active','is_default')


CHOICES = [('1','')]
EditResults = modelformset_factory(AttendanceCoursePoints, fields=('id','course_id','category','points','description','is_active','is_default'),
                                   can_delete=True, widgets={'is_active':CheckboxInput(),'is_default':RadioSelect(choices=CHOICES)})

EditUser= modelformset_factory(AttendanceUser,fields=('id','course_id','is_present','is_leave','is_absent','is_excused','comment','user_name','user_email'),
                               can_delete=True,widgets={'is_present':RadioSelect(choices=CHOICES),'is_leave':RadioSelect(choices=CHOICES),
                               'is_excused':RadioSelect(choices=CHOICES),'is_absent':RadioSelect(choices=CHOICES),
                                                        'comment':TextInput()})


